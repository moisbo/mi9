'use strict';

const http  = require('http');
const settings = require('./settings')();
const filter = require('./filter');
const log = require('./logger')();

var server = http.createServer(function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST');

    let data = '';
    req.on('data', function (chunk) {
        data += chunk;
    });

    req.on('end', function() {
        filter
            .parse(data)
            .then(function(response) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify(response));
                res.end();
            })
            .catch(function(){
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write('{"error":"Could not decode request: JSON parsing failed"}');
                res.end();
            });
    });

    req.on('error', function() {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write('{"error":"Could not decode request: JSON parsing failed"}');
        res.end();
    });
});

server.listen(process.env.PORT || settings.port, function(){
    log.info('Api listening on ', server.address().port);
});