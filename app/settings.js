'use strict';

module.exports = function() {
    return {
        port: 3000,
        url: 'http://localhost',
        logLevel: 'DEBUG',
        logFile: './app/log.out'
    };
};