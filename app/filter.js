'use strict';

const log = require('./logger')();

module.exports.parse = function(json){
    return new Promise(function(resolve, reject) {
        try {
            var valid = JSON.parse(json);
            var response = [];
            for(let i = 0; i < valid.payload.length; i++){
                if((valid.payload[i].drm === true) && (valid.payload[i].episodeCount > 0)){
                    let element = {};
                    element.image = valid.payload[i].image.showImage;
                    element.slug = valid.payload[i].slug;
                    element.title = valid.payload[i].title;
                    response.push(element);
                }
            }
            resolve({response: response});
        }catch(e){
            log.error(e);
            reject(null);
        }
    });
};
