'use strict';

const supertest = require('supertest');
const settings = require('../app/settings')();
const assert = require('assert');
const fs = require('fs');
const api = supertest(settings.url + ":" + settings.port);

describe('Test REST-API', function () {
    it('Should POST *', function (done) {
        let request = fs.readFileSync(process.cwd() + '/test-data/sample_request.json').toString();
        let response = fs.readFileSync(process.cwd() + '/test-data/sample_response.json');
        api.post('/')
            .set('Accept','application/json')
            .send(request)
            .expect(200)
            .end(function (err, res) {
                assert.deepEqual(res.body, JSON.parse(response));
                done();
            });
    });
    it('Should POST * and get error', function (done) {
        let string = '{"foo": "\\bar"}';
        api.post('/')
            .set('Accept','application/json')
            .send(string)
            .expect(400)
            .end(function (err, res) {
                done();
            });
    });
});