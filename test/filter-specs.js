'use strict';

const filter = require('../app/filter.js');
const fs = require('fs');
const assert = require('assert');

describe('Test Filter', function () {
    it('should validate JSON', function (done) {
        filter
            .parse('{"foo": "bar"}')
            .then(function(response){
                assert.equal(response, {});
                done();
            })
            .catch(function(reject){
                assert.equal(reject, null);
                done();
            });
    });
    it('Should filter to {}', function (done) {
        filter
            .parse('{"foo": "\\bar"}dd')
            .then(function(response){
                assert.equal(response, {});
                done();
            })
            .catch(function(reject){
                assert.equal(reject, null);
                done();
            });
    });
    it('Should parse file', function(done){
        let request = fs.readFileSync(process.cwd() + '/test-data/sample_request.json').toString();
        let response = fs.readFileSync(process.cwd() + '/test-data/sample_response.json');
        filter
            .parse(request)
            .then(function(res){
                assert.deepEqual(res, JSON.parse(response));
                done();
            })
            .catch(function(reject){
                assert.equal(reject, null);
                done();
            });
    });
});