'use strict';

const gulp = require('gulp');
const jshint = require('gulp-jshint');
const jscs = require('gulp-jscs');

const config = require('./gulp.config')();

gulp.task('lint', function () {
    return gulp
        .src(config.alljs)
        .pipe(jscs())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {verbose: true}));
});

gulp.task('test-filter', ['lint'], function (done) {
    startTest('test/filter-specs.js', done);
});

gulp.task('test-api', function (done) {
    startTest('test/api-specs.js', done);
});

function startTest(test, done) {
    const mocha = require('gulp-mocha');
    gulp.src(test, {read: false})
        .pipe(mocha({reporter: 'spec'}));
    done();
}

gulp.task('serve-dev', ['test-filter','lint'], function () {
    const nodemon = require('gulp-nodemon');
    nodemon({
        script: config.start,
        ext: 'js',
        delay: 1000,
        ignore: config.ignoreFiles
    })
        .on('restart', function (files) {
            console.log('Change detected:', files);
        });
});

gulp.task('watch', function () {
    gulp.watch(config.alljs, ['lint']);
});

gulp.task('default', ['watch']);
