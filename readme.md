**Welcome**

This is my solution to the mi9 code challenge. It is written in Node.JS with minimum dependencies for understanding of core concepts of the `http` module.

To install:

`npm install`

To run a development environment:

`gulp serve-dev` Will execute a test of the filter and then serve in port 3000

To test:

- `gulp test-filter` Will run a test of app/filter.js.

- `gulp test-api` Will run a test of the REST-API once it's started.

Or to run all of the test please run:

`mocha`

